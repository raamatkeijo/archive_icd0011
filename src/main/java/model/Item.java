package model;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class Item {

    @Column(name = "item_name")
    @NotNull
    private String itemName;

    @NotNull
    @Min(1)
    private int quantity;

    @NotNull
    @Min(1)
    private int price;
}
