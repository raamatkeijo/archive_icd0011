package config;

import config.sec.handlers.ApiAccessDeniedHandler;
import config.sec.handlers.ApiEntryPoint;
import config.sec.jwt.JwtAuthenticationFilter;
import config.sec.jwt.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutFilter;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@PropertySource("classpath:/application.properties")
@ComponentScan(basePackages = {"config"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource datasource;

    @Value("${jwt.signing.key}")
    private String jwtKey;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.authorizeRequests().antMatchers("/api/login").permitAll();
        http.authorizeRequests().antMatchers("/api/version").permitAll();
//        http.authorizeRequests()
//                .antMatchers("/api/users/**").hasRole("ADMIN");
        http.authorizeRequests()
                .antMatchers("/api/users").hasRole("ADMIN");
        http.authorizeRequests().antMatchers("/api/**").authenticated();

        http.exceptionHandling()
                .authenticationEntryPoint(new ApiEntryPoint());

        http.exceptionHandling()
                .accessDeniedHandler(new ApiAccessDeniedHandler());

        http.logout().logoutUrl("/api/logout");

        var apiLoginFilter = new JwtAuthenticationFilter(authenticationManager(), "/api/login", jwtKey);
        http.addFilterAfter(apiLoginFilter, LogoutFilter.class);

        var jwtAuthFilter = new JwtAuthorizationFilter(authenticationManager(), jwtKey);
        http.addFilterBefore(jwtAuthFilter, LogoutFilter.class);

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(datasource)
                .passwordEncoder(new BCryptPasswordEncoder());
    }


}
