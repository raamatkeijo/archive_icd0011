package app;

import model.Order;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrdersController {

    private OrdersDAO dao;

    public OrdersController(OrdersDAO dao) {
        this.dao = dao;
    }

    @GetMapping("/api/version")
    public void version(){
    }

    @GetMapping("/api/orders")
    public List<Order> getOrders(){
        return dao.getAllOrders();
    }

    @GetMapping("/api/orders/{id}")
    public Order getOrderById(@PathVariable Long id){
        return dao.getOrderById(id);
    }

    @DeleteMapping("/api/orders/{id}")
    public void deleteOrder(@PathVariable Long id){
        dao.deleteOrderById(id);
    }

    @PostMapping("/api/orders")
    @ResponseStatus(HttpStatus.CREATED)
    public Order createOrder(@RequestBody @Valid Order order){
        return dao.insertOrder(order);
    }

    @GetMapping("/api/users/{userName}")
    @PreAuthorize("hasRole('ADMIN') || #userName == authentication.name")
    public void userByName(@PathVariable String userName) {
    }
    @GetMapping("/api/users")
    public void users() {
    }
}
