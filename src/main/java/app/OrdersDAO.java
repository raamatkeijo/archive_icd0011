package app;

import model.Order;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class OrdersDAO {

    @PersistenceContext
    private EntityManager em;


    @Transactional
    public Order insertOrder(Order order){
        if (order.getId() == null){
            em.persist(order);
        } else {
            em.merge(order);
        }
        return order;
    }

    @Transactional
    public Order getOrderById(Long orderId){
        TypedQuery<Order> query = em.createQuery(
                "select o from Order o where o.id = :id", Order.class);
        query.setParameter("id", orderId);
        return query.getSingleResult();
    }

    public List<Order> getAllOrders() {
        return em.createQuery("select o from Order o").getResultList();
    }

    public void deleteOrderById(Long orderId) {
        Order order = getOrderById(orderId);
        if (order != null){
            em.remove(order);
        }
    }
}
