INSERT INTO orders (order_number) VALUES ('ACF4321B'), ('RF489FS');

INSERT INTO order_rows (row_id, item_name, quantity, price, order_id)
VALUES (1, 'CPU', 5, 245, 1), (2, 'Keyboard', 3, 78, 2), (3, 'Mothership', 2, 143, 1);

INSERT INTO USERS (USERNAME, PASSWORD, ENABLED, FIRST_NAME)
VALUES ('user', '$2a$11$bBBoXM/ibIw3WQPefs.IIeZXakEcTqRK7IbFdROReVZvZKZ0d7YZC', true, 'Voldemar'),
       ('admin', '$2a$11$wIkH6jMh6wR8dBMakVD/jOt8sDqJkQ4G.CtgeuY42YG9NyqO7G0pK', true, 'Voldemort');

INSERT INTO AUTHORITIES (USERNAME, AUTHORITY) VALUES
    ('user', 'ROLE_USER'),
    ('admin', 'ROLE_ADMIN');