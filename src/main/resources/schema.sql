DROP TABLE IF EXISTS order_rows;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS AUTHORITIES;
DROP TABLE IF EXISTS USERS;

DROP SEQUENCE IF EXISTS seq1;

CREATE SEQUENCE seq1 START WITH 1;


CREATE TABLE orders (
                        id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
                        order_number VARCHAR(255) NOT NULL
);

CREATE TABLE order_rows (

                            id BIGINT NOT NULL PRIMARY KEY DEFAULT nextval('seq1'),
    item_name VARCHAR(255),
                            price INT,
                            quantity INT,
                            orders_id BIGINT,
                            FOREIGN KEY (orders_id)
                                REFERENCES orders ON DELETE CASCADE
);

CREATE TABLE USERS (
                       username VARCHAR(255) NOT NULL PRIMARY KEY,
                       password VARCHAR(255) NOT NULL,
                       enabled BOOLEAN NOT NULL,
                       first_name VARCHAR(255) NOT NULL
);

CREATE TABLE AUTHORITIES (
                             username VARCHAR(50) NOT NULL,
                             authority VARCHAR(50) NOT NULL,
                             FOREIGN KEY (username) REFERENCES USERS
                                 ON DELETE CASCADE
);
CREATE UNIQUE INDEX IF NOT EXISTS ix_auth_username ON authorities (username, authority);

INSERT INTO USERS (USERNAME, PASSWORD, ENABLED, FIRST_NAME)
VALUES ('user', '$2a$11$bBBoXM/ibIw3WQPefs.IIeZXakEcTqRK7IbFdROReVZvZKZ0d7YZC', true, 'Voldemar'),
       ('admin', '$2a$11$wIkH6jMh6wR8dBMakVD/jOt8sDqJkQ4G.CtgeuY42YG9NyqO7G0pK', true, 'Voldemort');

INSERT INTO AUTHORITIES (USERNAME, AUTHORITY) VALUES
                                                  ('user', 'ROLE_USER'),
                                                  ('admin', 'ROLE_ADMIN');